#!/usr/bin/env node

const fs = require('fs')

const date = process.env['CI_PIPELINE_CREATED_AT'].substring(0, 10);
console.log('date:', date);

const calendar = JSON.parse(fs.readFileSync('calendar.json'))
const events = calendar[date] || []

const eventText = () => {
    switch (events.length) {
        case 0:
            return 'gar kein Tag'
        case 1:
            return events[0]
        case 2:
            return `${events[0]} und ${events[1]}`
        default:
            return `${events.slice(0, -1).join(', ')} und ${events[events.length - 1]}`
    }
}

const formatDay = () => {
    const numDays = Math.floor(
        (new Date() - new Date(date))
        / 24 / 60 / 60 / 1000
    )

    if (numDays > 1) {
        return `Vor ${numDays} Tagen war`;
    }

    if (numDays === 1) {
        return 'Gestern war'
    }

    return 'Heute ist';
};

const payload = {
    text: `${formatDay()} ${eventText()}. #Fredkalender
https://www.fonflatter.de/kalender/`
}
fs.writeFileSync('payload.json', JSON.stringify(payload))
